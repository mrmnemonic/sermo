/*
 * sermo
 *
 * Copyright (c) 2018 Martin A. COLEMAN (martin@mchomenet.com)
 * Released under the MIT license.
 *
 * Inspired by DimSum. Designed for CC-CEDICT data.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

GtkWidget *entry_Search_English;
GtkWidget *entry_Search_LOTE;
GtkWidget *entry_Result1;
GtkWidget *entry_Result2;
GtkWidget *entry_Result3;

GtkTextBuffer *textBuffer;

GtkTextIter tv_start, tv_end;

#include "lookup.c"

void search_english(GtkWidget *widget, gpointer window)
{
	gchar *str;
    const gchar *str_English;

    str_English=gtk_entry_get_text(GTK_ENTRY(entry_Search_English));
	str = g_strdup_printf("%s button clicked", gtk_button_get_label(GTK_BUTTON(widget)));
	printf("English: %s\nSearch [%s]\n", str, str_English);
    if(strlen(str_English)>0)
    {
        gtk_entry_set_text(GTK_ENTRY(entry_Search_LOTE), "");
        //gtk_entry_set_text(GTK_ENTRY(entry_Result1), "");
        //gtk_entry_set_text(GTK_ENTRY(entry_Result2), "");
        //gtk_entry_set_text(GTK_ENTRY(entry_Result3), "");
        gtk_text_buffer_set_text(textBuffer, "", 1);
        find_definition_ENGLISH(str_English);
    }
	g_free(str);
}

void search_lote(GtkWidget *widget, gpointer window)
{
	gchar *str;
    const gchar *str_LOTE;

    str_LOTE=gtk_entry_get_text(GTK_ENTRY(entry_Search_LOTE));
	str = g_strdup_printf("%s button clicked", gtk_button_get_label(GTK_BUTTON(widget)));
	printf("LOTE: %s\nSearch [%s]\n", str, str_LOTE);
    gtk_entry_set_text(GTK_ENTRY(entry_Search_English), "");
    gtk_text_buffer_set_text(textBuffer, "", 1);
    find_definition_LOTE(str_LOTE);
	g_free(str);
}

void sermo_quit(GtkWidget *widget, gpointer window)
{
    lookup_database_init();
    gtk_main_quit();
}

int main(int argc, char *argv[])
{
	GtkWidget *window;
	GtkWidget *table;
    GtkWidget *vbox;
	
    GtkWidget *btn_Search_English;
    GtkWidget *btn_Search_LOTE;

	GtkWidget *lbl_Search_English;
	GtkWidget *lbl_Search_LOTE;
    GtkWidget *lbl_Result1;
    GtkWidget *lbl_Result2;
    GtkWidget *lbl_Result3;
    
    // TextView
    GtkWidget *view;

	gtk_init(&argc, &argv);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Sermo");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 700);
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);

	/* build our window */
	table = gtk_table_new(5, 3, FALSE);
	gtk_container_add(GTK_CONTAINER(vbox), table);

	/* Add labels */
	lbl_Search_English = gtk_label_new("English");
	lbl_Search_LOTE = gtk_label_new("LOTE");
	lbl_Result1 = gtk_label_new("Result 1");
	lbl_Result2 = gtk_label_new("Result 2");
	lbl_Result3 = gtk_label_new("Result 3");

	gtk_table_attach(GTK_TABLE(table), lbl_Search_English,
		0, 1, 0, 1, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
	gtk_table_attach(GTK_TABLE(table), lbl_Search_LOTE,
		0, 1, 1, 2, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

	/* Language results */
	//gtk_table_attach(GTK_TABLE(table), lbl_Result1,
	//	0, 1, 2, 3, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
	//gtk_table_attach(GTK_TABLE(table), lbl_Result2,
	//	0, 1, 3, 4, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
	//gtk_table_attach(GTK_TABLE(table), lbl_Result3,
	//	0, 1, 4, 5, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

	/* Add text entry */
	entry_Search_English = gtk_entry_new();
	entry_Search_LOTE = gtk_entry_new();
	entry_Result1 = gtk_entry_new();
	entry_Result2 = gtk_entry_new();
	entry_Result3 = gtk_entry_new();
	gtk_table_attach(GTK_TABLE(table), entry_Search_English,
		1, 2, 0, 1, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
	gtk_table_attach(GTK_TABLE(table), entry_Search_LOTE,
		1, 2, 1, 2, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);	
	
	/* Language results */
	//gtk_table_attach(GTK_TABLE(table), entry_Result1,
	//	1, 2, 2, 3, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);	
	//gtk_table_attach(GTK_TABLE(table), entry_Result2,
	//	1, 2, 3, 4, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);	
	//gtk_table_attach(GTK_TABLE(table), entry_Result3,
	//	1, 2, 4, 5, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);	

	/* Add buttons */
	btn_Search_English = gtk_button_new_with_label("Search");
	gtk_widget_set_size_request(btn_Search_English, 70, 30);
	btn_Search_LOTE = gtk_button_new_with_label("Search");
	gtk_widget_set_size_request(btn_Search_LOTE, 70, 30);

	gtk_table_attach(GTK_TABLE(table), btn_Search_English,
		2, 3, 0, 1, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);
	gtk_table_attach(GTK_TABLE(table), btn_Search_LOTE,
		2, 3, 1, 2, GTK_FILL | GTK_SHRINK, GTK_FILL | GTK_SHRINK, 5, 5);

	g_signal_connect(G_OBJECT(btn_Search_English), "clicked", G_CALLBACK(search_english), NULL);
	g_signal_connect(G_OBJECT(btn_Search_LOTE), "clicked", G_CALLBACK(search_lote), NULL);
	//g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(window, "destroy", G_CALLBACK(sermo_quit), NULL);

    // vertical box
    view = gtk_text_view_new();
    gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(view), GTK_WRAP_WORD);
    gtk_box_pack_start(GTK_BOX(vbox), view, TRUE, TRUE, 0);
    gtk_widget_grab_focus(view);
    textBuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view));

    /* let's get busy */
    gtk_widget_show_all(window);
    lookup_database_init();
    if(!load_file("cedict_ts.u8"))
    {
        printf("Error loading dictionary file.\n");
        return 1;
    }

	gtk_main();
	return 0;
}
