LIBS=`pkg-config --cflags --libs gtk+-2.0`

all: sermo

sermo:
	gcc -o sermo main.c ${LIBS}

lookup:
	gcc -DSTANDALONE -o lookup lookup.c

clean:
	-rm -f sermo lookup
