/*
 * lookup.c
 * By Martin Coleman (c) 2018. Released under the MIT license.
 *
 */
#ifdef STANDALONE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#endif

#define DEF_LEN 128
#define MAX_DEFINITIONS 125000

typedef struct {
    char chinese[DEF_LEN];
    char pinyin[DEF_LEN];
    char english1[DEF_LEN];
    char english2[DEF_LEN];
    char english3[DEF_LEN];
} MYWORDS;

MYWORDS cedict[MAX_DEFINITIONS];
int number_of_records=0;

int string_parse(char *str, int record)
{
	int i,j,cnt,inword;
	char word_token[64][64];
	char sub_cmd[128];

	// Is it a comment?
	if(str[0] == '#') {
		//printf("%s\n", str);
		return 0;
	}

    /* initialise word_token array */
    /* MC-TODO maybe move this to start of program */
	for(i=0;i<64;i++)
	{
		memset(word_token[i], '\0', 64);
	}

    j=0;cnt=0,inword=0;
	for (i = 0; i <= strlen(str); i++)
	{
		if(str[i]=='/' ||  str[i]=='\0')
		{
			if(!inword)
			{
				word_token[cnt][j]='\0';
				cnt++;
				j=0;
			} else {
				word_token[cnt][j]=' ';
				j++;
			}
		/*
        } else if (str[i]=='[') {
            //word_token[cnt][j]=str[i];
            //cnt++;
            j=0;
            inword=1;
        } else if (str[i]==']') {
            word_token[cnt][j]='\0';
            cnt++;
            j=0;
            inword=0;
            //i++;
        */
		} else {
			word_token[cnt][j]=str[i];
			j++;
		}
	}
    for (i = 0; i < cnt; i++)
	{
        if(strlen(word_token[i])>1) {
            #ifdef DEBUG
            printf("WORD: %d [%s]\n", i, word_token[i]);
            #endif
        }
	}
    i=record;
    strcpy(cedict[i].chinese, word_token[0]);
    //strcpy(cedict[i].pinyin, word_token[1]);
    strcpy(cedict[i].english1, word_token[1]);
    strcpy(cedict[i].english2, word_token[2]);
    strcpy(cedict[i].english3, word_token[3]);
    return 1;
}

int find_definition_ENGLISH(const char *str)
{
    int i;
    int show_all=0;
    int show_pinyin=0;

    char *definition_buffer;

    printf("Searching for: [%s]\n", str);
    for(i=0;i<number_of_records;i++)
    {
        //printf("Searching:\nPhrase: %s\nPhrase: %s\nPhrase: %s\n", cedict[i].english1, cedict[i].english2, cedict[i].english3);

        if(show_all)
        {
            if(strstr(cedict[i].english1, str) || strstr(cedict[i].english2, str) || strstr(cedict[i].english3, str))
            {
                if(show_pinyin)
                {
                    printf("Chinese: %s\nPinyin: %s\nEnglish: %s\nEnglish: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].pinyin, cedict[i].english1, cedict[i].english2, cedict[i].english3);
                } else {
                    printf("Chinese: %s\nEnglish: [%s]\nEnglish: [%s]\nEnglish: [%s]\n\n", cedict[i].chinese, cedict[i].english1, cedict[i].english2, cedict[i].english3);
                    gtk_text_buffer_get_start_iter (textBuffer, &tv_start);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].chinese, strlen(cedict[i].chinese));
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].english1, -1);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);

                    if(strlen(cedict[i].english2)>0)
                    {
                        gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].english2, -1);
                        gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                        gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);
                    }

                    if(strlen(cedict[i].english3)>0)
                    {
                        gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].english3, -1);
                        gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                        gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);
                    }
                }
            }
        } else {
            if(strstr(cedict[i].english1, str))
            {
                if(show_pinyin)
                {
                    printf("Chinese: %s\nPinyin: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].pinyin, cedict[i].english1);
                } else {
                    printf("Chinese: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].english1);
                    //gtk_entry_set_text(GTK_ENTRY(entry_Search_LOTE), cedict[i].chinese);
                    //gtk_entry_set_text(GTK_ENTRY(entry_Result1), cedict[i].english1);

                    /* Obtain iters for the start and end of points of the buffer */
                    gtk_text_buffer_get_start_iter (textBuffer, &tv_start);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].chinese, -1);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].english1, -1);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", -1);
                }
            }
        }
    }
}

int find_definition_LOTE(const char *str)
{
    int i;
    int show_all=0;
    int show_pinyin=0;

    printf("Searching for: [%s]\n", str);
    for(i=0;i<number_of_records;i++)
    {
        //printf("Searching:\nPhrase: %s\nPhrase: %s\nPhrase: %s\n", cedict[i].english1, cedict[i].english2, cedict[i].english3);

        if(show_all)
        {
            if(strstr(cedict[i].english1, str) || strstr(cedict[i].english2, str) || strstr(cedict[i].english3, str))
            {
                if(show_pinyin)
                {
                    printf("Chinese: %s\nPinyin: %s\nEnglish: %s\nEnglish: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].pinyin, cedict[i].english1, cedict[i].english2, cedict[i].english3);
                } else {
                    printf("Chinese: %s\nEnglish: %s\nEnglish: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].english1, cedict[i].english2, cedict[i].english3);
                }
            }
        } else {
            if(strstr(cedict[i].chinese, str))
            {
                if(show_pinyin)
                {
                    printf("Chinese: %s\nPinyin: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].pinyin, cedict[i].english1);
                } else {
                    printf("Chinese: %s\nEnglish: %s\n\n", cedict[i].chinese, cedict[i].english1);
                    //gtk_entry_set_text(GTK_ENTRY(entry_Search_LOTE), cedict[i].chinese);
                    //gtk_entry_set_text(GTK_ENTRY(entry_Result1), cedict[i].english1);

                    /* Obtain iters for the start and end of points of the buffer */
                    gtk_text_buffer_get_start_iter (textBuffer, &tv_start);
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].chinese, strlen(cedict[i].chinese));
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", strlen("\n"));

                    gtk_text_buffer_insert( textBuffer, &tv_end, cedict[i].english1, strlen(cedict[i].english1));
                    gtk_text_buffer_get_end_iter (textBuffer, &tv_end);
                    gtk_text_buffer_insert( textBuffer, &tv_end, "\n", strlen("\n"));
                }
            }
        }
    }
}

void lookup_database_init(void)
{
    int i;

    /* initialise structure */
    printf("Initialising dictionary structure: ");
    for(i = 0; i < MAX_DEFINITIONS; i++)
    {
        memset(cedict[i].chinese, '\0', DEF_LEN);
        memset(cedict[i].pinyin, '\0', DEF_LEN);
        memset(cedict[i].english1, '\0', DEF_LEN);
        memset(cedict[i].english2, '\0', DEF_LEN);
        memset(cedict[i].english3, '\0', DEF_LEN);
    }
    printf("Done.\n");
}

int load_file(char *filename)
{
    int i,n=0;
    FILE *fp;
    char line[1024];

    fp=fopen(filename, "r");
    if(fp==NULL) {
        printf("Error opening dictionary file.\n");
        return 0;
    }
    while (fgets(line, sizeof(line), fp))
    {
        /* note that fgets don't strip the terminating \n, checking its
           presence would allow to handle lines longer that sizeof(line) */
        if(line[0] != '#')
        {
            //remove newline at end
            if(line[strlen(line)-1] = '\n'){
                line[strlen(line)-1]='\0';
            }

            /*
            // remove trailing slash
            if(line[strlen(line)-2] = '/'){
                line[strlen(line)-2]='\0';
            }

            // remove trailing slash
            if(line[strlen(line)-1] = ' '){
                line[strlen(line)-1]='\0';
            }
            */

            #ifdef DEBUG
            printf("Original: %s\n", line);
            #endif
            string_parse(line, n);
            //printf("\n");
        }
        n++;
        //if(n>50) break;
    }
    fclose(fp);
    number_of_records=n;
    printf("Parsed: %d records.\n", n);
    return 1;
}

#ifdef STANDALONE
int main(int argc, char *argv[])
{
    char *phrase;
    phrase=strdup(argv[1]);

    if (argc<2)
    {
        printf("Please enter phrase to search for.\n");
        return 1;
    }
    lookup_database_init();
    if(!load_file("cedict_ts.u8"))
    {
        printf("Error loading dictionary file.\n");
        return 1;
    }
    find_definition_ENGLISH(phrase);
    return 0;
}
#endif
